﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace poc.HttpApi.Client.ConsoleTestApp
{
    [DependsOn(
        typeof(pocHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class pocConsoleApiClientModule : AbpModule
    {
        
    }
}
