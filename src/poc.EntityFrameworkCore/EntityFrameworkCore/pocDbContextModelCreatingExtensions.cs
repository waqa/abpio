using Microsoft.EntityFrameworkCore;
using Volo.Abp;

namespace poc.EntityFrameworkCore
{
    public static class pocDbContextModelCreatingExtensions
    {
        public static void Configurepoc(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            /* Configure your own tables/entities inside here */

            //builder.Entity<YourEntity>(b =>
            //{
            //    b.ToTable(pocConsts.DbTablePrefix + "YourEntities", pocConsts.DbSchema);
            //    b.ConfigureByConvention(); //auto configure for the base class props
            //    //...
            //});
        }
    }
}