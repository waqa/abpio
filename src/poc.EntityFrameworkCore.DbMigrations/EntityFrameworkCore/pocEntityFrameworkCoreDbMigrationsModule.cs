﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace poc.EntityFrameworkCore
{
    [DependsOn(
        typeof(pocEntityFrameworkCoreModule)
    )]
    public class pocEntityFrameworkCoreDbMigrationsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<pocMigrationsDbContext>();
        }
    }
}