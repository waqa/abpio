﻿using poc.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace poc.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(pocEntityFrameworkCoreDbMigrationsModule),
        typeof(pocApplicationContractsModule)
    )]
    public class pocDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options =>
            {
                options.IsJobExecutionEnabled = false;
            });
        }
    }
}